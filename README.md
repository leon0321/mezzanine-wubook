Mezzanine Wubook
==================

Integration with wubook iframe and form

Features
--------

 - Integration with wubook iframe
 - Integration with wubook form


How to Use
----------

 1. Get and install the package:

        git clone git@bitbucket.org:naritas/mezzanine-wubook.git mezzanine-wubook

        cd mezzanine-wubook

        python setup.py install

    Mezzanine 3 or higher is required.

 2. Install the app in your Mezzanine project by adding
    `mezzanine_wubook` to the list of `INSTALLED_APPS` in your
    project's `settings.py`.

 3. Add Wubook Page with the mezzanine admin pages..

 4. Example Use wubook form in html.

        {% load mezzanine_wubook_tags %}

        {% get_wubook_static_files 'css' %}

        {% include 'mezzanine_wubook/includes/form_wubook_inline.html' %}

        <script src="{% static "/path/js/jquery" %}"></script>
        {% get_wubook_static_files 'js' %}

 5. add in your file settings.py.

        DATE_INPUT_FORMATS = ['%d/%m/%Y']

 6. configure module parameters in mezzanine settings

        WUBOOK_ACTION_FORM = ''     # Default (url of wubook page)


License
-------

Licence: BSD. See included `LICENSE` file.

Note that this license applies to this repository only.