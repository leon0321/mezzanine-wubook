from setuptools import setup, find_packages

import os

if os.path.exists("README.txt"):
    readme = open("README.txt")
else:
    print "Warning: using markdown README"
    readme = open("README.md")

setup(
    name="mezzanine-wubook",
    version="0.5",
    description="Integration with wubook iframe",
    long_description=readme.read(),
    author="Luis Velez",
    author_email="lvelezsantos@gmail.com",
    license="BSD",
    url="https://bitbucket.org/lvelezsantos/mezzanine-wubook/",
    install_requires=(
        "mezzanine >= 3",
        "south",
    ),
    packages=['mezzanine_wubook'],
    include_package_data=True,
    zip_safe=False,
    classifiers=[
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Framework :: Django",
    ],
)
