# coding=utf-8

from django.db import models
from django.utils.translation import ugettext_lazy as _

from mezzanine.pages.models import Page
from mezzanine.core.models import RichText


WOOBOOK_THEMES = (
    ('default', 'Default'),
    ('ammende', 'ammende'),
    ('asphalt', 'asphalt'),
    ('atlantic', 'atlantic'),
    ('autosole', 'autosole'),
    ('aventinn', 'aventinn'),
    ('avrora', 'avrora'),
    ('bear', 'bear'),
    ('compact', 'compact'),
    ('demetra', 'demetra'),
    ('dianapalace', 'dianapalace'),
    ('dostoevsky', 'dostoevsky'),
    ('forest', 'forest'),
    ('hm', 'hm'),
    ('hotelpeople', 'hotelpeople'),
    ('hotelsteam', 'hotelsteam'),
    ('marshal', 'marshal'),
    ('nautilusinn', 'nautilusinn'),
    ('octaviana', 'octaviana'),
    ('orangewhite', 'orangewhite'),
    ('paraiso', 'paraiso'),
    ('purple', 'purple'),
    ('pushkin', 'pushkin'),
    ('residencemoika', 'residencemoika'),
    ('serious', 'serious'),
    ('shelfort', 'shelfort'),
    ('sofi', 'sofi'),
    ('symfony', 'symfony'),
    ('triangle', 'triangle'),
    ('vergaz', 'vergaz'),
    ('wcdonald', 'wcdonald'),
    ('wubook', 'wubook'),
    ('wugle', 'wugle'),
    ('zak', 'zak'),
    ('zizi', 'zizi'),
)


WUBOOK_LANG = (
    ('ca', 'ca'),
    ('cs', 'cs'),
    ('de', 'de'),
    ('ee', 'ee'),
    ('en', 'en'),
    ('es', 'es'),
    ('fi', 'fi'),
    ('fr', 'fr'),
    ('gr', 'gr'),
    ('hr', 'hr'),
    ('it', 'it'),
    ('ko', 'ko'),
    ('lv', 'lv'),
    ('nl', 'nl'),
    ('pt', 'pt'),
    ('ro', 'ro'),
    ('ru', 'ru'),
    ('uk', 'uk'),
)


WUBOOK_MOBILE = (
    (0, _('No')),
    (1, _('Yes')),
)


class WubookPage(Page, RichText):

    class_content = models.CharField(
        verbose_name=_('Class content'),
        max_length=50,
    )

    lcode = models.CharField(
        verbose_name=_('Property code'),
        max_length=50,
    )

    show_left_column = models.BooleanField(
        verbose_name=_('Show left column'),
        default=False
    )

    show_right_column = models.BooleanField(
        verbose_name=_('Show right column'),
        default=False
    )

    theme = models.CharField(
        verbose_name=_('Booking theme'),
        max_length=50,
        choices=WOOBOOK_THEMES,
    )

    mobile = models.SmallIntegerField(
        verbose_name=_('Open in new window in mobile devices'),
        choices=WUBOOK_MOBILE,
        default=0,
    )

    lang = models.CharField(
        verbose_name=_('Language'),
        max_length=5,
        choices=WUBOOK_LANG,
        blank=True
    )

    width = models.PositiveIntegerField(
        verbose_name=_('Width'),
        help_text=_('Value in pixels (px). To stop automatic leave the field value 0'),
        default=0
    )

    height = models.PositiveIntegerField(
        verbose_name=_('Height'),
        help_text=_('Value in pixels (px). To stop automatic leave the field value 0'),
        default=0
    )

    class Meta:
        verbose_name = _('Wubook Page')
        verbose_name_plural = _('Wubook Pages')