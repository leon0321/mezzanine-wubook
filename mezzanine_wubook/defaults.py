from django.utils.translation import ugettext as _
from mezzanine.conf import register_setting
#
# register_setting(
#     name="WUBOOK_MAX_NIGHTS",
#     description=_("Max Nights in wubook form"),
#     editable=True,
#     default=30,
# )

register_setting(
    name="WUBOOK_ACTION_FORM",
    description=_("Url for action in wubook form"),
    editable=True,
    default='',
)