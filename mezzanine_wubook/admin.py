# coding=utf-8
from copy import deepcopy

from django.contrib import admin
from django.utils.translation import ugettext as _

from mezzanine_wubook.models import WubookPage
from mezzanine.pages.admin import PageAdmin


wubookpage_extra_fields = (
    (
        _('Wubook Iframe Configuration'),
        {
            "fields": (
                ("content",),
                ("lcode", ),
                ("show_left_column", 'show_right_column', ),
                ("theme", 'lang', ),
                ("width", 'height', ),
                ('mobile', )
            )
        }
    ),
)


class WubookPageAdmin(PageAdmin):
    pass
    #fieldsets = deepcopy(PageAdmin.fieldsets) + wubookpage_extra_fields

admin.site.register(WubookPage, WubookPageAdmin)
